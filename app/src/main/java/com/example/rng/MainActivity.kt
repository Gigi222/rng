package com.example.rng

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init() {
        generateRandomNumberButton.setOnClickListener {
            val number: Int = randomNumber()
            d("randomNumber", "This is random number $number")
            divisibleBy5(number)
            randomNumberTextView.text = number.toString()

        }
    }
    private fun randomNumber() = (-100..100).random()

    fun divisibleBy5(number:Int){
        if (number % 5 == 0) {
            Indicator.text = "Yes"
        } else {
            Indicator.text = "No"
        }
    }
}